/**
  Copyright (c) 2015, 2020, Oracle and/or its affiliates.
  The Universal Permissive License (UPL), Version 1.0
*/
define(['ojs/ojcomposite', 'text!./login-page-view.html', './login-page-viewModel', 'text!./component.json', 'css!./login-page-styles'],
  function(Composite, view, viewModel, metadata) {
    Composite.register('login-page', {
      view: view,
      viewModel: viewModel,
      metadata: JSON.parse(metadata)
    });
  }
);